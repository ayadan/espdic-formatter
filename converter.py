textFile = open( "espdic.txt", "r" )
csvFile = open( "espdic.csv", "w" )

# Format:
# -i : to (infinitive ending)

csvFile.write( "esperanto,english\n" )

for count, line in enumerate( textFile ):
    cols = line.split( ":" )
    esperanto = cols[0].strip( " " )
    english = cols[1].strip( " " )
    english = english.strip( "\r\n" )

    esperanto = esperanto.replace( "\"", "\\\"" )
    english = english.replace( "\"", "\\\"" )
    
    print( english, esperanto )
    csvFile.write( "\"" + esperanto + "\",\"" + english + "\"\n" )
